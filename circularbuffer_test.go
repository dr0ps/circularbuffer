package circularbuffer

import (
	"testing"
	"time"
)

const queueLen = 250

func TestCircularBuffer(t *testing.T) {
	q, err := NewCircularBuffer[int](1)
	if err == nil {
		t.Fatal(`NewCircularBuffer() must be error`)
	}

	q, err = NewCircularBuffer[int](queueLen)
	if err != nil {
		t.Fatal(`NewCircularBuffer() must be created without error`)
	}

	if !q.IsEmpty() {
		t.Fatal(`IsEmpty() must be true`)
	}

	if q.TotalSize() != queueLen {
		t.Fatalf(`TotalSize() must be equal %d, but TotalSize = %d`, queueLen, q.TotalSize())
	}

	for i := 1; i < queueLen+1; i++ {
		q.Push(i)
	}

	time.Sleep(2 * time.Second)

	if !q.IsFull() {
		t.Fatalf(`IsFull() must be true, but CurrentSize = %d`, q.CurrentSize())
	}

	val := <-q.Read()
	if val != 1 {
		t.Fatalf(`Pop() value must be equal 1 but val = %d`, val)
	}

	if q.CurrentSize() != queueLen-1 {
		t.Fatalf(`CurrentSize() must be equal %d, but CurrentSize = %d`, queueLen-1, q.CurrentSize())
	}

	q.Push(26)
	q.Push(27)

	if !q.IsFull() {
		t.Fatal(`IsFull() must be true`)
	}

	val = <-q.Read()
	if val != 3 {
		t.Fatalf(`Pop() value must be equal 3 but val = %d`, val)
	}

	if q.IsFull() {
		t.Fatal(`IsFull() must be false`)
	}

	for i := 0; i < queueLen-1; i++ {
		<-q.Read()
	}

	if !q.IsEmpty() {
		t.Fatal(`IsEmpty() must be true`)
	}
}

func BenchmarkCircularBufferPushRead(b *testing.B) {
	q, err := NewCircularBuffer[int](queueLen)
	if err != nil {
		b.Fatal(err)
	}

	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < queueLen; i++ {
		q.Push(i)
		<-q.Read()
	}
}
