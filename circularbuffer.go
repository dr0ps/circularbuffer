package circularbuffer

import (
	"fmt"
	"sync"
)

const minCap = 3

type CircularBuffer[T any] struct {
	inCh  chan T
	outCh chan T
	wg    *sync.WaitGroup
}

func NewCircularBuffer[T any](size int) (*CircularBuffer[T], error) {
	if size < minCap {
		return nil, fmt.Errorf("CircularBuffer size must be greater than %d", minCap)
	}

	cb := &CircularBuffer[T]{
		inCh:  make(chan T),
		outCh: make(chan T, size),
		wg:    &sync.WaitGroup{},
	}

	cb.wg.Add(1)
	go cb.Run()

	return cb, nil
}

func (b *CircularBuffer[T]) Push(data T) (err error) {
	defer func() {
		if x := recover(); x != nil {
			err = fmt.Errorf("%v", x)
		}
	}()

	b.inCh <- data
	return err
}

func (b *CircularBuffer[T]) Read() chan T {
	return b.outCh
}

func (b *CircularBuffer[T]) Stop() (err error) {
	defer func() {
		if x := recover(); x != nil {
			err = fmt.Errorf("%v", x)
		}
	}()

	close(b.inCh)
	b.wg.Wait()

	return nil
}

func (b *CircularBuffer[T]) Run() {
	defer b.wg.Done()

	for v := range b.inCh {
		select {
		case b.outCh <- v:
		default:
			<-b.outCh
			b.outCh <- v
		}
	}

	close(b.outCh)
}

// Next are the thread-unsafe methods (for metrics)
func (b *CircularBuffer[T]) IsEmpty() bool {
	return len(b.outCh) == 0
}

func (b *CircularBuffer[T]) IsFull() bool {
	return len(b.outCh) == cap(b.outCh)
}

func (b *CircularBuffer[T]) TotalSize() int {
	return cap(b.outCh)
}

func (b *CircularBuffer[T]) CurrentSize() int {
	return len(b.outCh)
}
